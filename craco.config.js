const packageName = require('./package.json').name;

module.exports = {
    webpack : {
       configure: {
            output: {
                library: `${packageName}-[name]`,
                libraryTarget: 'umd',
                chunkLoadingGlobal:  `webpackJsonp_${packageName}`,
                globalObject: 'window'
            }
        }
    },
    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        historyApiFallback: true,
        hot: false,
        liveReload: false,
        port: 7100 // This can be setup using the .env file feature from CRA
    }

};

