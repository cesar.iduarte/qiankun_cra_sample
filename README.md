# Create React App
Sample CRA@5.0.1 app adjusted to work with qiankun

## How to use it
#### Clone the repo
```
git clone git@gitlab.com:cesar.iduarte/qiankun_cra_sample.git
```

#### Install dependencies
```
npm i
```

#### Start the development server
```
npm start
```

## Qiankun Configuration Steps
### 1. Add public-path.(t|j)s

Create the plugin-path.(j|t)s file in the src directory with the following content:
```
if (window.__POWERED_BY_QIANKUN__) {
  __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__;
}
```

### 2. Set the base of history mode routing
Modify the App render method to include the following configuration
```
<BrowserRouter basename={window.__POWERED_BY_QIANKUN__ ? '/react-app' : '/'}>
```

### 3. Adjust the entry file (generally index.ts)
Modify the entry file to expose the lifecycle methods.
```javascript

// This is needed to render the app when loading it as a standalone app (localhost:<app_port>)
if (!window.__POWERED_BY_QIANKUN__) {
  render({});
}

// The code bellow is for a react 16 app.
// The App.tsx in the current project has an example for a react 18 app.

function render(props) {
  const { container } = props;
  ReactDOM.render(<App />, container ? container.querySelector('#root') : document.querySelector('#root'));
}

if (!window.__POWERED_BY_QIANKUN__) {
  render({});
}

export async function bootstrap() {
  console.log('[react16] react app bootstraped');
}

export async function mount(props) {
  console.log('[react16] props from main framework', props);
  render(props);
}

export async function unmount(props) {
  const { container } = props;
  ReactDOM.unmountComponentAtNode(container ? container.querySelector('#root') : document.querySelector('#root'));
}
```

### 4. Adjust the Webpack configuration
To adjust the webpack configuration we will use the craco package, the rescripts/cli mentioned in the qiankun site does not support react-scripts>@5

#### Install craco
```
npm i @craco/craco -D
```

#### Create craco.config.js
Outside the src directory create a craco.config.js empty file and add the following:
```javascript
const packageName = require('./package.json').name;

module.exports = {
    webpack : {
       configure: {
            output: {
                library: `${packageName}-[name]`,
                libraryTarget: 'umd',
                chunkLoadingGlobal:  `webpackJsonp_${packageName}`,
                globalObject: 'window'
            }
        }
    },
    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        historyApiFallback: true,
        hot: false,
        liveReload: false,
        port: 7100 // This can be setup using the .env file feature from CRA
    }

};
```
Modify it accordingly if needed.
### 5. Adjust the package.json

Modify the package.json to replace react-scripts with craco.
```json
-   "start": "react-scripts start",
+   "start": "craco start",
-   "build": "react-scripts build",
+   "build": "craco build",
-   "test": "react-scripts test",
+   "test": "craco test",
```