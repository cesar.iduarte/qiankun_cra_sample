import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App__header">
      App running on port 7100
      </header>
    </div>
  );
}

export default App;
