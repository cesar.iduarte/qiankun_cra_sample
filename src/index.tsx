import './public-path';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

let root: ReactDOM.Root;


if (!window.__POWERED_BY_QIANKUN__) {
  render({});
}


function render(props: any) {
  const { container } = props;

  root = ReactDOM.createRoot(
    container ? container.querySelector('#root') : document.querySelector('#root') as HTMLElement
  );

  root.render(
    <React.StrictMode>
       <BrowserRouter basename={window.__POWERED_BY_QIANKUN__ ? '/react-app' : '/'}>
        <App />
       </BrowserRouter>
    </React.StrictMode>
  );
}

export async function bootstrap() {
  console.log('[react16] react app bootstraped');
}

export async function mount(props: any) {
  console.log('[react16] props from main framework', props);
  render(props);
}

export async function unmount(props: any) {
  const { container } = props;
  root.unmount();
}